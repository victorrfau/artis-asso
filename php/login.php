<?php
require_once "./config.php";
session_start();

$query = 'select password, artisanat from users where email = :email ';
$errors = [];
$stmt = $pdo->prepare($query);
$stmt->execute([
    ":email" => $_POST['login']
]);
$request = $stmt->fetch();

if ($request == false){
    array_push($errors, "login does not  exists");
    $_SESSION["errors"] = $errors;
    header("Location: ../html/signin.php");
    exit;
}

if ($request['artisanat'] == "user"){
    array_push($errors, "Vous n'etes pas un artisant");
    $_SESSION["errors"] = $errors;
    header("Location: ../html/signin.php");
    exit;
}
$hash = $request["password"];
if (password_verify($_POST["password"], $hash) == true){
    $query = 'select nom, name, email, deplace, description, profile, artisanat, search from users where email = :email ';

    $stmt = $pdo->prepare($query);
    $stmt->execute([
        ":email" => $_POST['login']
    ]);
    $request = $stmt->fetchAll();
    $_SESSION["connected"] = true;
    $_SESSION['user'] = $request[0];
    header('Location: ../index.php');
    exit;
}else{
    array_push($errors, 'wrong login or password');
    $_SESSION["errors"] = $errors;
    header('Location: ../html/signin.php');
    exit;
}
