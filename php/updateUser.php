<?php

session_start();
require_once 'config.php';

define('target', '../images/profiles/');
define('maxSize', '100000');
define('maxWidth', "1000000");
define('maxHeigh', '1000000');
define('extension', "jpg");

$errors = [];
$deplace = 0;
$search = 0;

if ($_POST["search"] == "on") {
    $search = 1;
}

if ($_POST["deplace"] == "on") {
    $deplace = 1;
}

/*
array (size=4)
  'artisanat' => string 'boulanger' (length=9)
  'deplace' => string 'on' (length=2)
  'search' => string 'on' (length=2)
  'description' => string 'Pour moi, la boulangerie c'est comme le gras, c'est la vie. 
Je suis la meilleur boulangère du monde, je te fais la meilleure baguette du monde. ' (length=147)
*/

if ($_FILES["myFile"]["name"] !== "") {



    $Extension = pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);
    $imgInfo = getimagesize($_FILES['myFile']['tmp_name']);

    if ($Extension !== extension) {
        array_push($errors, "only jpeg");
    }
    if (($imgInfo[0] <= maxWidth) && ($imgInfo[1] <= maxHeigh) && (filesize($_FILES['myFile']['tmp_name']) >= maxSize)) {
        array_push($errors, "size problem");
    }

    if (sizeof($errors) > 0){
        $_SESSION["errors"] = $errors;
        header('Location: ../index.php');
        exit;

    }

    $mail = $_SESSION["user"]["email"];

    $query = "select profile from users where email = '$mail' ";
    $stmt = $pdo->query($query)->fetch();

    if ($stmt !== null){
        $file = $stmt["profile"];
        unlink("../images/profiles/" . $file);
    }

    $nomImg = uniqid() . "." . extension;
    var_dump($_FILES['myFile']['tmp_name']);
    try{
        move_uploaded_file($_FILES['myFile']['tmp_name'], target.$nomImg);
    }catch (Exception $e){
        echo $e;
        die();
    }
var_dump("Not uploaded because of error #".$_FILES["myFile"]["error"]);

    $sql = "UPDATE users SET artisanat=:name, deplace=:deplace, search=:search, profile=:img, description=:description WHERE email=:mail";
    $stmt = $pdo->prepare($sql)->execute([
        "name" => $_POST["artisanat"],
        "deplace" => $deplace,
        "search" => $search,
        "description" => $_POST["description"],
        "mail" => $_SESSION["user"]["email"],
        "img" => $nomImg
    ]);
}else{
    $sql = "UPDATE users SET artisanat=:name, deplace=:deplace, search=:search, description=:description WHERE email=:mail";
    $stmt = $pdo->prepare($sql)->execute([
        "name" => $_POST["artisanat"],
        "deplace" => $deplace,
        "search" => $search,
        "description" => $_POST["description"],
        "mail" => $_SESSION["user"]["email"]
    ]);
}



header('Location: ../index.php');
exit;

