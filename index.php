<!DOCTYPE html>
<html lang="en">
<?php
include_once './php/getUser.php';
if ($_SESSION["connected"] !== true or $_SESSION["artisant"] == "user"){
    header("Location: ./html/signin.php");
    exit();
}

?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Artis Associate</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- core CSS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates&display=swap" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/owl.carousel.min.css" rel="stylesheet">
    <link href="css/icomoon.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.png">

</head>
<!--/head-->

<body class="homepage">

<header id="header">


    <nav class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="images/logo-black.png" alt="logo"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav text-black artis-small-font">
                    <li class="active"><a href="index.php">Profil</a></li>
                    <li><a href="html/contact-us.php">Contact</a></li>
                </ul>
            </div>
        </div>
        <!--/.container-->
    </nav>
    <!--/nav-->

</header>
<!--/header-->


<form action="./php/updateUser.php" method="post" enctype="multipart/form-data">
    <section id="feature">
        <div class="container">
            <div class="center fadeInDown">
                <?php
                $profile = $request["profile"]
                ?>
                <img class="profile-pic" src="./images/profiles/<?=$profile?>"<br/>
                <input type="file" name="myFile" style="display: initial; !important;">
                <h3><?=$request["nom"] . " " . $request["name"]?></h3>
                <h5><input type="text" name="artisanat" value="<?= $request["artisanat"] ?>"></h5>
                <hr/>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-6 col-sm-12 animation animated-item-1" data-wow-duration="1000ms"
                         data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <h2>Disponibilté</h2>
                            <div class="round">
                                <input type="checkbox" id="checkbox" name="deplace"
                                    <?php
                                        if ($request['deplace'] == 1)
                                            echo 'checked'
                                    ?>
                                />
                                <label for="checkbox">Disponible à l'étranger</label>
                            </div>
                            <div class="round">
                                <input type="checkbox" id="checkbox" name="search"
                                    <?php
                                    if ($request['search'] == 1)
                                        echo 'checked'
                                    ?>
                                />
                                <label for="checkbox">A la recherche de contrat</label>
                            </div>
                        </div>
                    </div>
                    <!--/.col-md-3-->
                    <div class="col-md-6 col-sm-12 animation animated-item-2" data-wow-duration="1000ms"
                         data-wow-delay="600ms">
                        <div class="feature-wrap">

                            <h2>Description</h2>
                            <p><textarea name="description" id="" cols="70" rows="5"><?= $request['description']?></textarea></p>
                        </div>
                    </div>

                </div>
                <!--/.services-->
            </div>
            <input type="submit" value="enregisrer le profile">
            <?php
            if (sizeof($_SESSION['errors']) > 0 ){
                foreach ($_SESSION['errors'] as $error){
                    echo '<P>' . $error . '</p>';
                }
                $_SESSION['errors'] = [];
            }
            ?>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <!--/#feature-->
</form>
<?php
include_once "html/assets/footer.php"
?>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/main.js"></script>
</body>

</html>
