<!DOCTYPE html>
<html lang="en">
<?php
session_start();
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Artis</title>

    <!-- core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">
    <link href="../css/owl.carousel.min.css" rel="stylesheet">
    <link href="../css/icomoon.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>

        <header id="header">
  

                <nav class="navbar navbar-inverse" role="banner">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="../index.php"><img src="../images/logo-black.png" alt="logo"></a>
                        </div>
        
                        <div class="collapse navbar-collapse navbar-right">
                            <ul class="nav navbar-nav artis-small-font">
                                <li class="active"><a href="../index.php">Home</a></li>
                                <li><a href="contact-us.php">Contact</a></li>
                            </ul>
                        </div>
        
                    </div>
                    <!--/.container-->
                </nav>
                <!--/nav-->
        
            </header>
            <!--/header-->

    <div class="page-title" style="background-image: url(../images/page-title.png)">
        <br/>
    </div>

    <section id="contact-page">
        <div class="container">
            <div class="text-center">        
                <h3>Marie Diaut</h3>       <!--NOM DE L'ARTISAN-->
                <p>Céramiste</p> <!--NOM DE L'ARTISAN-->
            </div>
            <div class="col-sm-4 col-sm-offset-3">
                <?php
                if (sizeof($_SESSION['errors']) > 0 ){
                    foreach ($_SESSION['errors'] as $error){
                        echo '<P>' . $error . '</p>';
                    }
                    $_SESSION['errors'] = [];
                }
                ?>
                <form action="../php/login.php" method="post">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="staticEmail" name="login">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
                        </div>
                    </div>
                    <input type="submit" value="connexion" class="btn btn-asso" style="border: none ">
                    <a class="btn btn-asso" href="./signup.php">inscription</a>
                </form>

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->


  <?php
  include_once "assets/footer.php"
  ?>
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.prettyPhoto.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/main.js"></script>
</body>

</html>