<section id="bottom">
    <div class="container fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="row">
            <div class="col-md-2">
                <a href="#" class="footer-logo">
                    <img src="https://artis-artisanat.com/images/logo.png" alt="logo">
                </a>
            </div>
            <div class="col-md-10">
                <div class="row text-white">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="widget">
                            <h3>Accès</h3>
                            <ul>
                                <?php if($_SESSION['connected'] == true) : ?>
                                    <li><a href="https://artis-artisanat.com/php/logout.php">Déconnexion</a></li>
                                <?php else : ?>
                                    <li><a href="https://artis-artisanat.com/html/signin.php">Connexion</a></li>
                                    <li><a href="https://artis-artisanat.com/html/signup.php">Inscription</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <!--/.col-md-3-->
                    <!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="widget">
                            <h3>Mentions</h3>
                            <ul>
                                <li><a href="https://artis-artisanat.com/html/cgu.php">CGU</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="widget">
                            <h3>Contacts</h3>
                            <ul>
                                <li><a href="https://www.facebook.com/Artisfrance">Facebook</a></li>
                                <li><a href="https://www.instagram.com/artis_france/">Insta</a></li>
                                <li><a href="https://twitter.com/artis_france">contact@artis-artisanat.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--/.col-md-3-->
                </div>
            </div>


        </div>
    </div>
</section>
<!--/#bottom-->

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="center-it col-sm-12">
                &copy; 2019 <a target="_blank" href="https://artis-artisanat.com" title="La plateforme d'artisans'">Artis</a> Copyright.
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->