<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Artis</title>
    

    <!-- core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">
    <link href="../css/owl.carousel.min.css" rel="stylesheet">
    <link href="../css/icomoon.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
        <header id="header">
                <nav class="navbar navbar-inverse" role="banner">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="../index.php"><img src="../images/logo-black.png" alt="logo"></a>
                        </div>
                        </div>
        
                    </div>
                    <!--/.container-->
                </nav>
                <!--/nav-->
        </header>
        <!--/header-->

    

    <section id="signup-page">
        <div class="container">
           <!-- multistep form -->
            <?php
            if (sizeof($_SESSION['errors']) > 0 ){
                foreach ($_SESSION['errors'] as $error){
                    echo '<P>' . $error . '</p>';
                }
                $_SESSION['errors'] = [];
            }
            ?>
<form id="msform" method="post" action="../php/inscription.php">
        <!-- progressbar -->
        <ul id="progressbar">
          <li class="active"></li>
          <li></li>
          <li></li>
        </ul>
        <!-- fieldsets -->
        <fieldset>
          <h2 class="fs-title">Vos infos</h2>
          <h3 class="fs-subtitle">Veuillez tout compléter</h3>
          <input type="text" name="fname" placeholder="Prénom" />
          <input type="text" name="lname" placeholder="Nom" />
          <input type="text" name="email" placeholder="Email" />
          <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Votre mot de passe</h2>
            <h3 class="fs-subtitle">Choisissez votre mot de passe</h3>
            <input type="password" name="pass" placeholder="Password" />
            <input type="password" name="cpass" placeholder="Confirm Password" />
          <input type="button" name="previous" class="previous action-button" value="Previous" />
          <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset>
          <h2 class="fs-title">Votre mot de passe</h2>
          <h3 class="fs-subtitle">Choisissez votre mot de passe</h3>
          <input type="text" name="siret" placeholder="N° SIRET" />
          <input type="text" name="social" placeholder="Raison sociale" />
          <div class="round">
                <input type="checkbox" required id="checkboxcgu" />
                <label for="checkboxcgu">J’accepte les conditions générales d’utilisation de Artis et de ses partenaires.</label>
            </div>
            <div class="round">
                <input type="checkbox" required id="checkboxrgpb" />
                <label for="checkboxrgpd">J’accepte de recevoir des emails d’information et de communication de la part de Artis et de ses partenaires.</label>
            </div>
          <input type="button" name="previous" class="previous action-button" value="Previous" />
          
          <input type="submit" name="submit" value="Submit" />
        </fieldset>
        
      </form>
        </div><!--/.container-->
    </section><!--/#contact-page-->



        <?php
        include_once "assets/footer.php"
        ?>

        <script src="../js/jquery.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.prettyPhoto.js"></script>
        <script src="../js/owl.carousel.min.js"></script>
        <script src="../js/jquery.isotope.min.js"></script>
    <script>//jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches
            
            $(".next").click(function(){
                if(animating) return false;
                animating = true;
                
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();
                
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                //show the next fieldset
                next_fs.show(); 
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50)+"%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                  });
                        next_fs.css({'left': left, 'opacity': opacity});
                    }, 
                    duration: 800, 
                    complete: function(){
                        current_fs.hide();
                        animating = false;
                    }, 
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            });
            
            $(".previous").click(function(){
                if(animating) return false;
                animating = true;
                
                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();
                
                //de-activate current step on progressbar
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                
                //show the previous fieldset
                previous_fs.show(); 
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1-now) * 50)+"%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({'left': left});
                        previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                    }, 
                    duration: 800, 
                    complete: function(){
                        current_fs.hide();
                        animating = false;
                    }, 
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            });
            
            $(".submit").click(function(){
                return false;
            })</script>
    <script src="../js/main.js"></script>
</body>

</html>